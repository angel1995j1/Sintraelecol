import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:social_share/social_share.dart';
import 'package:video_player/video_player.dart';
import '../elements/DrawerWidget.dart';

class ProcesosOrganizacionales extends StatefulWidget {
  ProcesosOrganizacionales({Key key}) : super(key: key);

  @override
  _ProcesosOrganizacionalesState createState() =>
      _ProcesosOrganizacionalesState();
}

class _ProcesosOrganizacionalesState extends State<ProcesosOrganizacionales> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );
    _initializeVideoPlayerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return new IconButton(
              icon: Icon(Icons.sort, color: Theme.of(context).hintColor),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            );
          },
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: Container(
                child: Padding(
              padding: const EdgeInsets.only(right: 50),
              child: Text(
                "Formación",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
            )),
          ),
          Divider(
            indent: 190,
            height: 10,
            thickness: 2,
            color: Colors.black,
          ),
          SizedBox(height: 20),
          Center(
              child: Text('Procesos Organizacionales',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))),
          SizedBox(height: 30),
          Container(
            height: 200,
            width: 300,
            child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tincidunt lacus vel elit fermentum iaculis ac quis eros. Nam vitae fringilla lacus. In aliquet maximus erat ut lacinia. Maecenas eget egestas augue, eget rhoncus arcu. Etiam et bibendum turpis. Praesent lobortis massa et congue laoreet. Ut sollicitudin ante tellus, at vestibulum tortor tempor eu.',
              textAlign: TextAlign.justify,
              overflow: TextOverflow.fade,
            ),
          ),
          SizedBox(height: 10),
          FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                // Si el VideoPlayerController ha finalizado la inicialización, usa
                // los datos que proporciona para limitar la relación de aspecto del VideoPlayer
                return Container(
                  height: 180,
                  width: 290,
                  child: AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    // Usa el Widget VideoPlayer para mostrar el vídeo
                    child: VideoPlayer(_controller),
                  ),
                );
              } else {
                // Si el VideoPlayerController todavía se está inicializando, muestra un
                // spinner de carga
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
          SizedBox(height: 25),
          Padding(
            padding: const EdgeInsets.only(right: 60),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  icon: Icon(FontAwesomeIcons.twitter,
                      size: 40, color: Colors.blue),
                  onPressed: () {
                    SocialShare.shareTwitter("Unidap",
                            hashtags: [
                              "SINTRAELECOL",
                              "appSocial",
                              "foo",
                              "bar"
                            ],
                            url: "https://google.com/#/hello",
                            trailingText: "\nhello")
                        .then((data) {
                      print(data);
                    });
                  },
                ),
                IconButton(
                  icon: Icon(FontAwesomeIcons.facebookSquare,
                      size: 40, color: Colors.blue[900]),
                  onPressed: () {
                    Platform.isAndroid
                        ? SocialShare.shareFacebookStory(
                                '../../../assets/img/logo.png',
                                "#ffffff",
                                "#000000",
                                "https://google.com",
                                appId: "xxxxxxxxxxxxx")
                            .then((data) {
                            print(data);
                          })
                        : SocialShare.shareFacebookStory(
                                '../../../assets/img/logo.png',
                                "#ffffff",
                                "#000000",
                                "https://google.com")
                            .then((data) {
                            print(data);
                          });
                  },
                )
              ],
            ),
          )
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (_controller.value.isPlaying) {
              _controller.pause();
            } else {
              _controller.play();
            }
          });
        },
        // Muestra el icono correcto dependiendo del estado del vídeo.
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ), // Esta coma final hace que el formateo automático sea mejor para los métodos de compilación.
    );
  }
}
