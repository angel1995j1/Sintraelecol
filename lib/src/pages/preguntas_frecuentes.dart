import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/preguntasFrecuentes_controller.dart';
import '../elements/CircularLoadingWidget.dart';

class PreguntasFrecuentes extends StatefulWidget {
  PreguntasFrecuentes({Key key}) : super(key: key);

  @override
  _PreguntasFrecuentesState createState() => _PreguntasFrecuentesState();
}

class _PreguntasFrecuentesState extends StateMVC<PreguntasFrecuentes> {
  PreguntasFrecuentesController _con;

  _PreguntasFrecuentesState() : super(PreguntasFrecuentesController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Preguntas frecuentes',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _con.onRefresh,
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              _con.preguntas.isEmpty
                  ? CircularLoadingWidget(height: 500)
                  : ListView.separated(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      primary: false,
                      itemCount: _con.preguntas.length,
                      separatorBuilder: (context, index) {
                        return Padding(
                          padding: EdgeInsets.only(top: 1, left: 10, right: 10),
                          child: Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                        );
                      },
                      itemBuilder: (context, indexFaq) {
                        return ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed(
                                  '/PreguntaDetalle',
                                  arguments:
                                      _con.preguntas.elementAt(indexFaq));
                            },
                            trailing: Icon(
                              FontAwesomeIcons.plus,
                              size: 18,
                              color: Colors.green.withOpacity(1),
                            ),
                            title: AutoSizeText(
                              _con.preguntas.elementAt(indexFaq).titulo,
                              maxFontSize: 15,
                              minFontSize: 13,
                              style: TextStyle(fontWeight: FontWeight.w700),
                            ));
                      },
                    )
            ],
          ),
        ),
      ),
    );
  }
}
