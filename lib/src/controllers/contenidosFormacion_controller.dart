import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../models/contenidoFormacion_model.dart';
import '../api/contenido_formacion_repository.dart' as repo;

class ContenidoFormacionController extends ControllerMVC {
  List<Contenidos> contenidos = <Contenidos>[];
  GlobalKey<ScaffoldState> scaffoldKey;

  ContenidoFormacionController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void getContenido() async {
    repo.getContenidoFormacion().then((value) => {
          print('INFORMACION ${value.contenidos.length}'),
          for (var data in value.contenidos)
            {
              setState(() {
                contenidos.add(data);
              })
            }
        });
  }
}
