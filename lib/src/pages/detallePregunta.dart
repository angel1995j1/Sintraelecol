import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import '../api/user_repository.dart';
import '../elements/DrawerWidget.dart';
import '../elements/PermisosDenegadosWidget.dart';
import '../models/preguntas_model.dart';

class PreguntaDetalle extends StatefulWidget {
  final Preguntas pregunta;

  PreguntaDetalle({Key key, this.pregunta}) : super(key: key);

  @override
  _PreguntaDetalleState createState() => _PreguntaDetalleState();
}

class _PreguntaDetalleState extends State<PreguntaDetalle> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: DrawerWidget(),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: Builder(
            builder: (BuildContext context) {
              return new IconButton(
                icon:
                    Icon(Icons.arrow_back, color: Theme.of(context).hintColor),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              );
            },
          ),
        ),
        body: !perfilUsuario.value.afiliado
            ? PermisosDenegados()
            : SingleChildScrollView(
                padding: EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                            child: Padding(
                          padding: const EdgeInsets.only(right: 50),
                          child: AutoSizeText(
                            widget.pregunta.titulo,
                            maxFontSize: 15,
                            minFontSize: 13,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        )),
                      ),
                    ),
                    Divider(
                      indent: 190,
                      height: 10,
                      thickness: 2,
                      color: Colors.black,
                    ),
                    SizedBox(height: 20),
                    Center(
                        child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: AutoSizeText(widget.pregunta.titulo,
                          maxFontSize: 15,
                          minFontSize: 13,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    )),
                    SizedBox(height: 30),
                    AutoSizeText(
                      widget.pregunta.detalles,
                      textAlign: TextAlign.justify,
                      overflow: TextOverflow.fade,
                    ),
                  ],
                ), //
              ));
  }
}
