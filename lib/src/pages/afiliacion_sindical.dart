import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:signature/signature.dart';
import '../controllers/afiliacion_controller.dart';
import '../elements/DrawerWidget.dart';
import '../models/afiliacion_model.dart';

var directoryName = 'Signature';

class AfiliacionSindicalPage extends StatefulWidget {
  final Afiliacion registroAfiliacion;

  AfiliacionSindicalPage({Key key, this.registroAfiliacion}) : super(key: key);

  @override
  _AfiliacionSindicalPageState createState() => _AfiliacionSindicalPageState();
}

class _AfiliacionSindicalPageState extends StateMVC<AfiliacionSindicalPage> {
  AfiliacionController _con;
  final SignatureController _controller = SignatureController(
    penStrokeWidth: 5,
    penColor: Colors.black,
    exportBackgroundColor: Colors.white,
  );

  bool isVendor = false;

  _AfiliacionSindicalPageState() : super(AfiliacionController()) {
    _con = controller;
    _controller.addListener(() => print('value changed'));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: Colors.white,
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text('Afiliación Sindical',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
        centerTitle: false,
        elevation: 0,
      ),
      body: SingleChildScrollView(
          child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Center(
                    child: Image.asset('./assets/img/logo.png',
                        scale: 1, width: 140),
                  )),
              Container(
                padding: EdgeInsets.only(top: 23.0),
                child: Column(
                  children: [
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                child: Form(
                                    child: Column(
                                  children: [
                                    Text(
                                      'Ingresa tu firma para completar tu afiliación sindical',
                                      style: TextStyle(
                                          color: Theme.of(context).accentColor,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(height: 10),
                                    Container(
                                      height: 190,
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: Colors.grey)),
                                      child: Signature(
                                        controller: _controller,
                                        height: 300,
                                        backgroundColor: Colors.white,
                                        key:
                                            null, // key that allow you to provide a GlobalKey that'll let you retrieve the image once user has signed
                                      ),
                                    ),
                                    FlatButton(
                                        onPressed: () {
                                          setState(() => _controller.clear());
                                        },
                                        child: Icon(
                                          FontAwesomeIcons.trash,
                                          size: 15,
                                          color: Colors.red,
                                        )),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    InkWell(
                                      onTap: () {},
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Checkbox(
                                            value: isVendor,
                                            activeColor:
                                                Theme.of(context).accentColor,
                                            checkColor: Colors.white,
                                            onChanged: (value) {
                                              setState(() {
                                                isVendor = !isVendor;
                                              });
                                            },
                                          ),
                                          SizedBox(width: 15),
                                          Flexible(
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        'Solicito comedidamente se admita mi afiliación a SINTRAELECOL que ustedes representan y me comprometo a cumplir con los estatutos y demás disposiciones legales del sindicato',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 10,
                                                        textAlign:
                                                            TextAlign.justify,
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {},
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Checkbox(
                                            value: isVendor,
                                            activeColor:
                                                Theme.of(context).accentColor,
                                            checkColor: Colors.white,
                                            onChanged: (value) {
                                              setState(() {
                                                isVendor = !isVendor;
                                              });
                                            },
                                          ),
                                          SizedBox(width: 15),
                                          Flexible(
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        'Acepto los Términos y condiciones',
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 10,
                                                        textAlign:
                                                            TextAlign.justify,
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                              ),
                              SizedBox(height: 10),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 45,
                                    width: 240,
                                    decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color:
                                                Colors.green.withOpacity(0.4),
                                            blurRadius: 10,
                                            offset: Offset(0, 7)),
                                        BoxShadow(
                                            color:
                                                Colors.green.withOpacity(0.2),
                                            blurRadius: 5,
                                            offset: Offset(0, 3))
                                      ],
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(40)),
                                    ),
                                    child: RaisedButton(
                                      color: Colors.green,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(7)),
                                      onPressed: () async {
                                        if (_controller.isNotEmpty) {
                                          var data =
                                              await _controller.toPngBytes();
                                          widget.registroAfiliacion
                                                  .firmaAfiliacion =
                                              base64.encode(data);
                                          _con.sendImageFirma(
                                              widget.registroAfiliacion,
                                              context);
                                        }
                                        // Navigator.of(context).pushReplacementNamed('/Home');
                                      },
                                      child: Text('AFILIARME',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          )
        ],
      )),
    );
  }
}
