import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../api/user_repository.dart';
import '../controllers/asesorias_controller.dart';
import '../elements/PermisosDenegadosWidget.dart';

class AsesoriasJuridicas extends StatefulWidget {
  AsesoriasJuridicas({Key key}) : super(key: key);

  @override
  _AsesoriasJuridicasState createState() => _AsesoriasJuridicasState();
}

class _AsesoriasJuridicasState extends StateMVC<AsesoriasJuridicas> {
  AsesoriasController _con;

  _AsesoriasJuridicasState() : super(AsesoriasController()) {
    _con = controller;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // key: _con.scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: BackButton(color: Colors.black87),
        ),
        body: !perfilUsuario.value.afiliado
            ? PermisosDenegados()
            : SingleChildScrollView(
                child: GestureDetector(
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Column(
                  children: [
                    Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 20, bottom: 20),
                          child: Text('Asesorias juridicas',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                        )),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(
                          child: Padding(
                        padding: const EdgeInsets.only(right: 50),
                        child: Text(
                          "Canal jurídico",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      )),
                    ),
                    Divider(
                      indent: 190,
                      height: 10,
                      thickness: 4,
                      color: Colors.black,
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Form(
                        key: _con.uploadAsesoriaFormKey,
                        child: Column(
                          children: [
                            SizedBox(height: 20),
                            TextFormField(
                              keyboardType: TextInputType.multiline,
                              maxLines: 3,
                              onSaved: (input) =>
                                  _con.asesoria.descripcion = input,
                              decoration: InputDecoration(
                                labelText: 'Describe tu caso',
                                labelStyle: TextStyle(color: Colors.green),
                                contentPadding: EdgeInsets.all(12),
                                hintText: 'Caso',
                                hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.7)),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.2))),
                              ),
                            ),
                            SizedBox(height: 20),
                            Container(
                                padding: EdgeInsets.all(20),
                                width: double.infinity,
                                height: 60,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                  border: Border.all(
                                    width: 1,
                                    color: Theme.of(context)
                                        .focusColor
                                        .withOpacity(0.2),
                                  ),
                                ),
                                alignment: Alignment.centerLeft,
                                child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                  hint: _con.asesoria.opciones == null
                                      ? Text('Selecciona una opción')
                                      : Text(
                                          _con.asesoria.opciones,
                                          style: TextStyle(color: Colors.green),
                                        ),
                                  isExpanded: true,
                                  iconSize: 30.0,
                                  style: TextStyle(color: Colors.green),
                                  items: [
                                    'Bloqueos por términos y condiciones',
                                    'Bloqueos por deudas',
                                    'Accidente de trabajo',
                                    'Trámites ante el sistema de seguridad social',
                                    'Vulneración a derechos fundamentales',
                                    'Demandas para solicitar vínculo laboral'
                                  ].map(
                                    (val) {
                                      return DropdownMenuItem<String>(
                                        value: val,
                                        child: Text(val),
                                      );
                                    },
                                  ).toList(),
                                  onChanged: (val) {
                                    setState(
                                      () {
                                        print('Value $val');
                                        _con.asesoria.opciones = val;
                                      },
                                    );
                                  },
                                ))),
                            SizedBox(height: 20),
                            GestureDetector(
                              onTap: () {
                                _con.selectFile();
                              },
                              child: Container(
                                  height: 55,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    border: Border.all(
                                      width: 1,
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.2),
                                    ),
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('Anexa Archivos',
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.w600)),
                                            Icon(Icons.archive, size: 30),
                                          ],
                                        ),
                                      )
                                    ],
                                  )),
                            ),
                            SizedBox(height: 10),
                            _con.fileSelected
                                ? Text('Archivo Seleccionado',
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold))
                                : Container(),
                            SizedBox(height: 30),
                            Center(
                              child: Container(
                                height: 45,
                                width: 240,
                                decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.green.withOpacity(0.4),
                                        blurRadius: 10,
                                        offset: Offset(0, 7)),
                                    BoxShadow(
                                        color: Colors.green.withOpacity(0.2),
                                        blurRadius: 5,
                                        offset: Offset(0, 3))
                                  ],
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(40)),
                                ),
                                child: RaisedButton(
                                  color: Colors.green,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(7)),
                                  onPressed: () {
                                    _con.subirPoliza(context);
                                  },
                                  child: _con.uploadFile
                                      ? Text('Cargando...',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold))
                                      : Text('Solicitar Asesoria',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold)),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )));
  }
}
