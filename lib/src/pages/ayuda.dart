import 'package:flutter/material.dart';

class AyudaPage extends StatefulWidget {
  AyudaPage({Key key}) : super(key: key);

  @override
  _AyudaPageState createState() => _AyudaPageState();
}

class _AyudaPageState extends State<AyudaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Ayuda'),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              child: Container(
                  child: Padding(
                padding: const EdgeInsets.only(right: 50),
                child: Text(
                  "Pagos",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              )),
            ),
            Divider(
              indent: 190,
              height: 10,
              thickness: 2,
              color: Colors.black,
            ),
          ],
        ));
  }
}
