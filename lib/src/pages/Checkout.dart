import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:payu_money_flutter/payu_money_flutter.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);

class CheckOutPage extends StatefulWidget {
  CheckOutPage({Key key}) : super(key: key);

  @override
  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  PayuMoneyFlutter payuMoneyFlutter = PayuMoneyFlutter();

  // Payment Details
  String phone = "8318045008";
  String email = "gmail@gmail.com";
  String productName = "My Product Name";
  String firstName = "Vaibhav";
  String txnID = "223428947";
  String amount = "1.0";

  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  _createPaymentIntent() async {
    startPayment();
    _btnController.reset();
  }

  @override
  void initState() {
    super.initState();
    setupPayment();
  }

  // Function for setting up the payment details
  setupPayment() async {
    bool response = await payuMoneyFlutter.setupPaymentKeys(
        merchantKey: "CA9nIRrB",
        merchantID: "53HWzcRBbU",
        isProduction: false,
        activityTitle: "App Title",
        disableExitConfirmation: false);

    print('TESPONSE $response');
  }

  // ignore: missing_return
  Future<Map<String, dynamic>> startPayment() async {
    var dio = Dio();
    // Generating hash from php server
    dynamic res = await dio
        .post("https://PayUMoneyServer.codedivinedivin.repl.co", data: {
      "txnid": txnID,
      "phone": phone,
      "email": email,
      "amount": amount,
      "productinfo": productName,
      "firstname": firstName,
    });
    await payuMoneyFlutter.startPayment(
        txnid: txnID,
        amount: amount,
        name: firstName,
        email: email,
        phone: phone,
        productName: productName,
        hash: 'hash');
    logger.i("Message ${res.data['status']}");
    _btnController.success();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text('Pago con tarjeta', style: TextStyle(color: Colors.black)),
        elevation: 0,
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            CreditCardWidget(
              cardNumber: cardNumber,
              expiryDate: expiryDate,
              cardBgColor: Colors.black,
              cardHolderName: cardHolderName,
              cvvCode: cvvCode,
              showBackView: isCvvFocused,
              obscureCardNumber: true,
              obscureCardCvv: true,
              labelCardHolder: 'Nombre completo',
              labelExpiredDate: 'MM/AA',
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    CreditCardForm(
                      formKey: formKey,
                      obscureCvv: true,
                      obscureNumber: true,
                      cardNumber: cardNumber,
                      cvvCode: cvvCode,
                      cardHolderName: cardHolderName,
                      expiryDate: expiryDate,
                      themeColor: Colors.blue,
                      cardNumberDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Número',
                        hintText: '•••• •••• ••••',
                      ),
                      expiryDateDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Expiración',
                        hintText: 'MM/AA',
                      ),
                      cvvCodeDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'CVV',
                        hintText: '•••',
                      ),
                      cardHolderDecoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Nombre completo',
                      ),
                      onCreditCardModelChange: onCreditCardModelChange,
                    ),
                    RoundedLoadingButton(
                      color: Theme.of(context).accentColor,
                      successColor: Theme.of(context).accentColor,
                      controller: _btnController,
                      onPressed: () => {
                        if (formKey.currentState.validate())
                          _createPaymentIntent()
                        else
                          Fluttertoast.showToast(
                              msg: 'Formulario inválido',
                              backgroundColor: Colors.red),
                        _btnController.reset()
                      },
                      valueColor: Colors.black,
                      borderRadius: 10,
                      child:
                          Text('Pagar', style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
