import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
// import 'package:unidapp/src/models/tareas_asignadas_model.dart';
import '../models/Task.dart';
import '../api/notas_repository.dart' as notasApi;
import '../api/user_repository.dart' as userApi;

class NotasController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  List<Tareas> tareas = <Tareas>[];
  bool loading = true;
  Tareas tareaDetalle = new Tareas();

  bool switched = false;
  bool checkedValue = false;

  NotasController() {
    obtenerTareas();
    profileUser();
  }

  void obtenerTareas() {
    notasApi.obtenerTareas().then((val) => {
          print('TAREAS'),
          setState(() {
            tareas = val.tareas;
          }),
        });
  }

  void tareaCompletada(id) {
    notasApi.tareaCompletada(id).then((res) => {print('response $res')});
  }

  void editarTarea(tarea) {
    tareaDetalle = tarea;
    notasApi.editarTarea(tarea).then((res) => {print('Value $res')});
  }

  profileUser() {
    loading = true;
    userApi
        .getInfoUser()
        .then((value) => {
              setState(() {
                print('Información del usuario ${value.pagado}');
                loading = false;
              })
            })
        .catchError((e) {
      loading = false;
    }).whenComplete(() => {loading = false});
  }
}
