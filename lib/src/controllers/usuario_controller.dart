import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../models/tu_sindicato_model.dart';
import '../utils/validators.dart';
import '../api/user_repository.dart' as repo;

class UserController extends ControllerMVC {
  String email,
      password,
      nombre,
      telefono,
      empresas = 'Rappi',
      sexo,
      nacionalidad,
      departamento;
  File image;
  bool hidePassword = true;
  bool loading = false;

  TuSindicato infoSindicato = new TuSindicato();

  GlobalKey<FormState> loginFormKey;

  GlobalKey<FormState> registerFormKey;

  GlobalKey<FormState> editarPerfilFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  UserController() {
    loginFormKey = new GlobalKey<FormState>();
    registerFormKey = new GlobalKey<FormState>();
    editarPerfilFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void login() async {
    loading = true;
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      repo
          .login(email: email, password: password)
          .then((val) => {
                if (val.token != null)
                  {
                    loading = false,
                    Navigator.of(scaffoldKey.currentContext)
                        .pushReplacementNamed('/Home')
                  },
                loading = false
              })
          .catchError((e) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text('Verifique sus credenciales'),
        ));
        setState(() {
          loading = false;
        });
      }).whenComplete(() {
        setState(() {
          loading = false;
        });
      });
    }
  }

  void registro() async {
    setState(() {
      loading = true;
    });
    if (registerFormKey.currentState.validate()) {
      registerFormKey.currentState.save();
      repo
          .registro(
              email: email,
              password: password,
              nombre: nombre,
              telefono: telefono,
              empresas: empresas,
              nacionalidad: nacionalidad,
              sexo: sexo,
              departamento: departamento)
          .then((value) => {
                if (value.estado)
                  {
                    Navigator.of(scaffoldKey.currentContext)
                        .pushReplacementNamed('/Login'),
                    showShortToast('Usuario creado con éxito')
                  },
              })
          .catchError((e) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text('Parece que el usuario ya se ha registrado'),
        ));
        setState(() {
          loading = false;
        });
      }).whenComplete(() {
        setState(() {
          loading = false;
        });
      });
    }
    setState(() {
      loading = false;
    });
  }

  void editarPerfil() async {
    if (editarPerfilFormKey.currentState.validate()) {
      editarPerfilFormKey.currentState.save();
      if (image != null) {
        try {
          String fileName = image.path.split('/').last;
          FormData formData = FormData.fromMap({
            "photo":
                await MultipartFile.fromFile(image.path, filename: fileName),
            "nombre": nombre,
            "telefono": telefono
          });
          dynamic response = await repo.editarUsuario(formData);
          Fluttertoast.showToast(
              msg: response,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0);
        } catch (e) {
          Fluttertoast.showToast(
              msg: 'Algo salió mal',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      }
      try {
        FormData formData =
            FormData.fromMap({"nombre": nombre, "telefono": telefono});
        dynamic response = await repo.editarUsuario(formData);
        Fluttertoast.showToast(
            msg: response,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);
      } catch (e) {
        Fluttertoast.showToast(
            msg: 'Algo salio mal',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    }
  }

  void obtenerPerfil() async {
    await repo.getInfoUser();
  }

  void loginWithFacebook() async {
    repo
        .singInWithEmailFacebook()
        .then((value) => {print('Response facebook $value')});
  }

  void getInfoSindicato() {
    repo.afiliacion().then((sindicato) => {
          setState(() {
            infoSindicato = sindicato;
          })
        });
  }
}
