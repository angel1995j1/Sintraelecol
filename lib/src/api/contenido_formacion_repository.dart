import 'dart:io';

import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';
import './user_repository.dart';
import '../models/contenidoFormacion_model.dart';

var dio = Dio();

Future getContenidoFormacion() async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}contenidoFormacion';
  try {
    Response response = await dio.get(url,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer ${currentUser.value.token}"
            }));
    return ContenidosFormacionModel.fromJson(response.data);
  } on DioError catch (e) {
    print('ERROR $e');
    return 'error';
  }
}
