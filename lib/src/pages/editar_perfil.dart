import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../api/user_repository.dart';
import '../controllers/usuario_controller.dart';

class EditarPefil extends StatefulWidget {
  EditarPefil({Key key}) : super(key: key);

  @override
  _EditarPefilState createState() => _EditarPefilState();
}

class _EditarPefilState extends StateMVC<EditarPefil> {
  UserController _con;
  final picker = ImagePicker();

  Future getItem() async {
    final pickedFile = await picker.getImage(
        source: ImageSource.gallery, maxHeight: 100, maxWidth: 100);
    setState(() {
      _con.image = File(pickedFile.path);
    });
  }

  _EditarPefilState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    print('oBTENER USUARIO');
    _con.obtenerPerfil();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
          title: Text('Editar perfil', style: TextStyle(color: Colors.black)),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed('/Home');
            },
          ),
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Container(
                            child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 25, bottom: 25, right: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Stack(
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          child: _con.image != null
                                              ? Image.asset(
                                                  _con.image.path,
                                                )
                                              : Image.network(
                                                  perfilUsuario.value.img,
                                                  width: 100,
                                                  height: 100,
                                                ),
                                        ),
                                        Positioned(
                                          top: 0,
                                          right: 0,
                                          child: GestureDetector(
                                            onTap: () {
                                              getItem();
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              child: Icon(Icons.edit,
                                                  color: Colors.white),
                                              decoration: BoxDecoration(
                                                color: Theme.of(context)
                                                    .accentColor,
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(20),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 34),
                                    Form(
                                      key: _con.editarPerfilFormKey,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          TextFormField(
                                            keyboardType:
                                                TextInputType.emailAddress,
                                            onSaved: (input) {},
                                            initialValue:
                                                perfilUsuario.value.email,
                                            validator: (input) =>
                                                !input.contains('@')
                                                    ? 'Correo inválido'
                                                    : null,
                                            decoration: InputDecoration(
                                              labelText: 'Correo',
                                              labelStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .accentColor),
                                              contentPadding:
                                                  EdgeInsets.all(12),
                                              hintText:
                                                  'torresroger445@gmail.com',
                                              hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .focusColor
                                                      .withOpacity(0.7)),
                                              prefixIcon: Icon(
                                                  Icons.alternate_email,
                                                  color: Theme.of(context)
                                                      .accentColor),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2))),
                                              focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.5))),
                                              enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2))),
                                            ),
                                          ),
                                          SizedBox(height: 20),
                                          TextFormField(
                                            keyboardType: TextInputType.text,
                                            initialValue:
                                                perfilUsuario.value.nombre,
                                            onSaved: (input) {
                                              _con.nombre = input;
                                            },
                                            validator: (input) =>
                                                input.length < 3
                                                    ? 'Nombre inválido'
                                                    : null,
                                            decoration: InputDecoration(
                                              labelText: 'Nombre',
                                              labelStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .accentColor),
                                              contentPadding:
                                                  EdgeInsets.all(12),
                                              hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .focusColor
                                                      .withOpacity(0.7)),
                                              prefixIcon: Icon(
                                                  FontAwesomeIcons.userCircle,
                                                  color: Theme.of(context)
                                                      .accentColor),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2))),
                                              focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.5))),
                                              enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2))),
                                            ),
                                          ),
                                          SizedBox(height: 20),
                                          TextFormField(
                                            keyboardType: TextInputType.number,
                                            initialValue:
                                                perfilUsuario.value.telefono,
                                            onSaved: (input) {
                                              _con.telefono = input;
                                            },
                                            validator: (input) =>
                                                input.length < 10
                                                    ? 'Nombre inválido'
                                                    : null,
                                            decoration: InputDecoration(
                                              labelText: 'Teléfono',
                                              labelStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .accentColor),
                                              contentPadding:
                                                  EdgeInsets.all(12),
                                              hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .focusColor
                                                      .withOpacity(0.7)),
                                              prefixIcon: Icon(
                                                  Icons.phone_iphone,
                                                  color: Theme.of(context)
                                                      .accentColor),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2))),
                                              focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.5))),
                                              enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2))),
                                            ),
                                          ),
                                          SizedBox(height: 30),
                                          Container(
                                            height: 45,
                                            width: 240,
                                            decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.green
                                                        .withOpacity(0.4),
                                                    blurRadius: 10,
                                                    offset: Offset(0, 7)),
                                                BoxShadow(
                                                    color: Colors.green
                                                        .withOpacity(0.2),
                                                    blurRadius: 5,
                                                    offset: Offset(0, 3))
                                              ],
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(40)),
                                            ),
                                            child: RaisedButton(
                                              color: Colors.green,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          7)),
                                              onPressed: () {
                                                _con.editarPerfil();
                                              },
                                              child: Text('Actualizar',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ))),
                      )
                    ],
                  ))
            ],
          ),
        )));
  }
}
