import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/usuario_controller.dart';

class RegistroPage extends StatefulWidget {
  RegistroPage({Key key}) : super(key: key);

  @override
  _RegistroPageState createState() => _RegistroPageState();
}

class _RegistroPageState extends StateMVC<RegistroPage> {
  UserController _con;

  _RegistroPageState() : super(UserController()) {
    _con = controller;
  }
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
        key: _con.scaffoldKey,
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 2.2,
              ),
              Column(
                children: [
                  Padding(
                      padding: EdgeInsets.only(top: 75),
                      child: Image.asset('./assets/img/logo.png',
                          width: 280, height: 70)),
                  Container(
                    padding: EdgeInsets.only(top: 30),
                    width: screenSize.width /
                        (2 / (screenSize.height / screenSize.width)),
                    child: Column(
                      children: [
                        Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            Container(
                                margin: EdgeInsets.only(bottom: 80),
                                width: 345,
                                child: Column(
                                  children: [
                                    SizedBox(height: 50),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Form(
                                          key: _con.registerFormKey,
                                          child: Column(
                                            children: [
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.emailAddress,
                                                onSaved: (input) =>
                                                    _con.email = input,
                                                validator: (input) =>
                                                    !input.contains('@')
                                                        ? 'Email inválido'
                                                        : null,
                                                decoration: InputDecoration(
                                                  labelText: 'Correo',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText:
                                                      'Correo electrónico',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(
                                                      FontAwesomeIcons.envelope,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              TextFormField(
                                                obscureText: _con.hidePassword,
                                                keyboardType:
                                                    TextInputType.emailAddress,
                                                onSaved: (input) =>
                                                    _con.password = input,
                                                validator: (input) =>
                                                    input.length < 3
                                                        ? 'Contraseña inválida'
                                                        : null,
                                                decoration: InputDecoration(
                                                  labelText: 'Contraseña',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText: '••••••••••••',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(
                                                      FontAwesomeIcons.asterisk,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  suffixIcon: IconButton(
                                                    onPressed: () {
                                                      setState(() {
                                                        _con.hidePassword =
                                                            !_con.hidePassword;
                                                      });
                                                    },
                                                    color: Theme.of(context)
                                                        .focusColor,
                                                    icon: Icon(_con.hidePassword
                                                        ? Icons.visibility
                                                        : Icons.visibility_off),
                                                  ),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.text,
                                                onSaved: (input) =>
                                                    _con.nombre = input,
                                                validator: (input) =>
                                                    input.length < 4
                                                        ? 'Nombre inválido'
                                                        : null,
                                                decoration: InputDecoration(
                                                  labelText: 'Nombre completo',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText:
                                                      'Ingresa tu nombre completo',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(
                                                      FontAwesomeIcons
                                                          .userCircle,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.phone,
                                                onSaved: (input) =>
                                                    _con.telefono = input,
                                                validator: (input) =>
                                                    input.length < 10
                                                        ? 'Número inválido'
                                                        : null,
                                                decoration: InputDecoration(
                                                  labelText: 'Telefono',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText: 'Teléfono',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(
                                                      Icons.phone_iphone,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              Container(
                                                  padding: EdgeInsets.all(20),
                                                  width: double.infinity,
                                                  height: 60,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(10),
                                                    ),
                                                    border: Border.all(
                                                      width: 2,
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2),
                                                    ),
                                                  ),
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child:
                                                      DropdownButtonHideUnderline(
                                                          child: DropdownButton(
                                                    hint: _con.sexo == null
                                                        ? Text('Sexo')
                                                        : Text(
                                                            _con.sexo,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green),
                                                          ),
                                                    isExpanded: true,
                                                    iconSize: 30.0,
                                                    style: TextStyle(
                                                        color: Colors.green),
                                                    items: [
                                                      'Mujer',
                                                      'Hombre',
                                                      'No Binario'
                                                    ].map(
                                                      (val) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          value: val,
                                                          child: Text(val),
                                                        );
                                                      },
                                                    ).toList(),
                                                    onChanged: (val) {
                                                      setState(
                                                        () {
                                                          print('Value $val');
                                                          _con.sexo = val;
                                                        },
                                                      );
                                                    },
                                                  ))),
                                              SizedBox(height: 20),
                                              Container(
                                                  padding: EdgeInsets.all(20),
                                                  width: double.infinity,
                                                  height: 60,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(10),
                                                    ),
                                                    border: Border.all(
                                                      width: 2,
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.2),
                                                    ),
                                                  ),
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child:
                                                      DropdownButtonHideUnderline(
                                                          child: DropdownButton(
                                                    hint: _con.sexo == null
                                                        ? Text('Empresa')
                                                        : Text(
                                                            _con.empresas,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green),
                                                          ),
                                                    isExpanded: true,
                                                    iconSize: 30.0,
                                                    style: TextStyle(
                                                        color: Colors.green),
                                                    items: [
                                                      'AFINIA',
                                                      'AIR-E',
                                                      'CENTRAL HIDROELECTRICA DE CALDAS SA ESP-CHEC',
                                                      'CENTRALES ELECTRICAS DE NARIÑO SA ESP-CEDENAR',
                                                      'CENTRALES ELECTRICAS DE NORTE DE SANTANDER S.A. E.S.P.-CENS',
                                                      'CODENSA',
                                                      'COMPAÑÍA DE ELECTRICIDAD DE TULÚA S.A. E.S.P.-CETSA',
                                                      'DELTEC',
                                                      'ELECTRIFICADORA DE CAQUETA S.A. E.S.P.-ELECTROCAQUETA',
                                                      'ELECTRIFICADORA DE SANTANDER S.A. E.S.P.-ESSA',
                                                      'ELECTRIFICADORA DEL HUILA S.A. E.S.P. -ELECTROHUILA',
                                                      'ELECTRIFICADORA DEL META E.S. E.S.P.-EMSA',
                                                      'EMGESA S.A. E.S.P.-EMGESA',
                                                      'EMPRESA ANTIOQUEÑA DE ENERGÍA-EADE',
                                                      'EMPRESA DE ENERGIA DE ARAUCA S.A. E.S.P.-ENERLAR',
                                                      'EMPRESA DE ENERGÍA DE BOYACA S.A. E.S.P.-EBSA',
                                                      'EMPRESA DE ENERGIA DEL GUAVIARE S.A. E.S.P. -ELECTROGUAVIARE',
                                                      'EMPRESA DE ENERGIA DEL PACIFICO S.A. E.S.P. -EPSA',
                                                      'EMPRESA DE ENERGIA DEL PUTUMAYO S.A. E.S.P. EEP',
                                                      'EMPRESA DE ENERGIA DEL QUINDIO E.S. E.S.P. EDEQ',
                                                      'ENECON',
                                                      'GENERADORA Y COMERCIALIZADORA DE ENERGIA DEL CARIBE S.A. E.S.P. GECELCA',
                                                      'GESTION ENERGETICA S.A E.S.P.-GENSA',
                                                      'INELMA',
                                                      'INMEL',
                                                      'INVERSIONES TERMOTASAJERO S.A. E.S.P-TERMOTASAJERO',
                                                      'MICOL',
                                                      'PROING',
                                                      'TRANSPORTADORA DE ENERGIA ELECTRICA DE LA COSTA ATLANTICA S.A. E.S.P. ISA-TRANSELCA',
                                                      'TRANSPORTADORA DE GAS INTERNACIONAL TGI S.A E.S.P.'
                                                    ].map(
                                                      (val) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          value: val,
                                                          child: val != null
                                                              ? Text(val)
                                                              : Text(_con
                                                                  .empresas),
                                                        );
                                                      },
                                                    ).toList(),
                                                    onChanged: (val) {
                                                      setState(
                                                        () {
                                                          _con.empresas = val;
                                                        },
                                                      );
                                                    },
                                                  ))),
                                              SizedBox(height: 20),
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.text,
                                                onSaved: (input) =>
                                                    _con.nacionalidad = input,
                                                decoration: InputDecoration(
                                                  labelText: 'Nacionalidad',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText: 'Nacionalidad',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(Icons.map,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.text,
                                                onSaved: (input) =>
                                                    _con.departamento = input,
                                                decoration: InputDecoration(
                                                  labelText:
                                                      'Departamento Ciudad',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText:
                                                      'Departamento Ciudad',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(
                                                      Icons.map_rounded,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 30),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    height: 45,
                                                    width: 240,
                                                    decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                            color: Colors.green
                                                                .withOpacity(
                                                                    0.4),
                                                            blurRadius: 10,
                                                            offset:
                                                                Offset(0, 7)),
                                                        BoxShadow(
                                                            color: Colors.green
                                                                .withOpacity(
                                                                    0.2),
                                                            blurRadius: 5,
                                                            offset:
                                                                Offset(0, 3))
                                                      ],
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  40)),
                                                    ),
                                                    child: RaisedButton(
                                                      color: Colors.green,
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                  .circular(7)),
                                                      onPressed: () {
                                                        setState(() {
                                                          if (!_con.loading) {
                                                            _con.registro();
                                                          }
                                                        });
                                                      },
                                                      child: _con.loading
                                                          ? CircularProgressIndicator(
                                                              backgroundColor:
                                                                  Colors.white,
                                                            )
                                                          : Text('Registrate',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          )),
                                    ),
                                  ],
                                )),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 0,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('¿Tienes una cuenta?'),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed('/Login');
                          },
                          child: Text(
                            " Iniciar sesión",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).accentColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
