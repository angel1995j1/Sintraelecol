import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:sweetalert/sweetalert.dart';
import '../models/afiliacion_model.dart';
import '../api/afiliacion_repository.dart' as repo;

class AfiliacionController extends ControllerMVC {
  Afiliacion afiliacion = new Afiliacion();
  bool directivo = false;
  Image firma;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> afiliacionFormKey;

  AfiliacionController() {
    afiliacionFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void registroAfiliacion(mycontext) {
    if (afiliacionFormKey.currentState.validate()) {
      afiliacionFormKey.currentState.save();
      Navigator.of(scaffoldKey.currentContext)
          .pushReplacementNamed('/AfiliacionSindical', arguments: afiliacion);
    }
  }

  void sendImageFirma(Afiliacion registroAfiliacion, mycontext) async {
    repo.registroAfiliacion(registroAfiliacion).then((val) => {
          if (val != null)
            {
              SweetAlert.show(mycontext,
                  title: "Formulario enviado",
                  subtitle: "Afiliación está en proceso de validación",
                  style: SweetAlertStyle.success),
              Future.delayed(Duration(seconds: 2), () {
                Navigator.of(scaffoldKey.currentContext)
                    .pushReplacementNamed('/Home');
              })
            }
          else
            {
              scaffoldKey?.currentState?.showSnackBar(SnackBar(
                content: Text('Verifique sus datos por favor'),
              ))
            },
          print('value $val')
        });
  }
}
