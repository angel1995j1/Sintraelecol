import 'package:dio/dio.dart';
import 'dart:io';
// import 'package:global_configuration/global_configuration.dart';
import './user_repository.dart';
import '../utils/helpers.dart';

var dio = Dio();

Future crearTarea(minuta) async {
  final uri = Helper.getUri('nueva-minuta');
  print('se obtiene los datos de la minuta $minuta');
  try {
    Response response = await dio.post(uri.toString(),
        data: minuta,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer ${currentUser.value.token}"
            }));
    print('Información --->>> ${response.statusCode}');
    return true;
  } on DioError catch (e) {
    print('error $e');
    return false;
  }
}
