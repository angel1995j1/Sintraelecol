import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jitsi_meet/jitsi_meet.dart';
import 'package:jitsi_meet/jitsi_meeting_listener.dart';
import '../api/user_repository.dart';
import '../elements/DrawerWidget.dart';
import '../elements/PermisosDenegadosWidget.dart';
import '../models/route_argument.dart';
import '../style/theme.dart';
import '../utils/edit_text_utils.dart';
import '../utils/helpers.dart';
import '../utils/jitsi_meet_utils.dart';
import '../utils/validators.dart';

import 'package:dio/dio.dart';

class UnirseMeetingShareWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  UnirseMeetingShareWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _UnirseMeetingShareWidgetState createState() =>
      _UnirseMeetingShareWidgetState();
}

class _UnirseMeetingShareWidgetState extends State<UnirseMeetingShareWidget> {
  GlobalKey<ScaffoldState> scaffoldState = GlobalKey();
  TextEditingController meetingIDController = new TextEditingController();
  var _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890'; //all characters
  Random _rnd = Random();
  String randomMeetingCode; //meeting room

  @override
  void initState() {
    super.initState();
    randomMeetingCode = getRandomString(9);
    JitsiMeet.addListener(JitsiMeetingListener(
        onConferenceWillJoin: JitsiMeetUtils().onConferenceWillJoin,
        onConferenceJoined: JitsiMeetUtils().onConferenceJoined,
        onConferenceTerminated: JitsiMeetUtils().onConferenceTerminated,
        onError: JitsiMeetUtils().onError));
  }

  //Regerating Random Meeting Code
  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  @override
  void dispose() {
    super.dispose();
    JitsiMeet.removeAllListeners();
  }

  _joinMeeting() async {
    try {
      var options = JitsiMeetingOptions()
        ..room = meetingIDController.text
        ..serverURL = 'https://meet.jit.si/'
        ..subject = 'SINTRAELECOL'
        ..userDisplayName = currentUser.value.usuario.nombre
        ..userEmail = currentUser.value.usuario.email
        ..audioOnly = true
        ..audioMuted = true
        ..videoMuted = true;
      await JitsiMeet.joinMeeting(options,
          listener: JitsiMeetingListener(onConferenceWillJoin: ({message}) {
            print('${options.room} will join with message: $message');
          }, onConferenceJoined: ({message}) {
            print('${options.room} joined with message: $message');
          }, onConferenceTerminated: ({message}) {
            print('${options.room} terminated with message: $message');
          }));
      _registrarMeeting(meetingIDController.text);
    } catch (e) {
      print('🚨Error meeting');
    }
  }

  _registrarMeeting(String idAsamblea) async {
    final uri = Helper.getUri('asamblea/$idAsamblea');
    dynamic body = {"asistentes": currentUser.value.usuario.email};

    try {
      dynamic response = await dio.put(
        uri.toString(),
        data: body,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          },
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}"
          },
        ),
      );
      print('response  agregando participantes $response');
    } on DioError catch (e) {
      print('ERROR AL REGISTRAR MEETING $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Asambleas', style: TextStyle(color: Colors.green)),
        elevation: 0,
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return new IconButton(
              icon: Icon(Icons.arrow_back_ios,
                  color: Theme.of(context).hintColor),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed('/Asamblea');
              },
            );
          },
        ),
      ),
      body: !perfilUsuario.value.afiliado
          ? PermisosDenegados()
          : SingleChildScrollView(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 20, top: 40, bottom: 50, right: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Sala de reuniones',
                                ),
                                SizedBox(height: 30.0),
                                /* user meeting title textField */
                                EditTextUtils().getCustomEditTextField(
                                    hintValue: 'ID de reunión',
                                    controller: meetingIDController,
                                    prefixWidget: Icon(FontAwesomeIcons.hashtag,
                                        size: 17, color: Colors.green),
                                    keyboardType: TextInputType.text,
                                    style: CustomTheme.textFieldTitle
                                        .merge(TextStyle(color: Colors.black)),
                                    validator: (value) {
                                      return validateNotEmpty(value);
                                    }),
                                SizedBox(height: 35),
                                GestureDetector(
                                  onTap: () {
                                    _joinMeeting();
                                  },
                                  child: Container(
                                    height: 45.0,
                                    decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(3.0)),
                                      border: Border.all(
                                          color: CustomTheme.primaryColor),
                                    ),
                                    child: Center(
                                        child: Text(
                                      'Unirte a reunión',
                                      style: CustomTheme.subTitleTextColored,
                                    )),
                                  ),
                                ),
                                SizedBox(height: 15.0),
                              ],
                            ),
                          ),
                          decoration: new BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            // boxShadow: CustomTheme.boxShadow,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
    );
  }
}
