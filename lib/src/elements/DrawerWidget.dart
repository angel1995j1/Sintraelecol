import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../api/user_repository.dart';
import '../api/user_repository.dart' as api;

class DrawerWidget extends StatefulWidget {
  DrawerWidget({Key key}) : super(key: key);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed('/EditarPerfil');
              },
              child: UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                ),
                accountName: Text(perfilUsuario.value.nombre,
                    style: Theme.of(context).textTheme.headline6),
                accountEmail: Text(perfilUsuario.value.email,
                    style: Theme.of(context).textTheme.caption),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Theme.of(context).accentColor,
                  backgroundImage: NetworkImage(perfilUsuario.value.img),
                ),
              )),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Home');
            },
            leading: Icon(
              Icons.home,
              color: Colors.grey,
            ),
            title: Text(
              'Inicio',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/AsesoriasJuridicas');
            },
            leading: Icon(
              FontAwesomeIcons.questionCircle,
              color: Colors.grey,
            ),
            title: Text(
              'Asesoría Jurídica',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/ContenidoFormacion');
            },
            leading: Icon(
              FontAwesomeIcons.idBadge,
              color: Colors.grey,
            ),
            title: Text(
              'Formación',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            trailing: Icon(Icons.chevron_right),
          ),
          perfilUsuario.value.afiliado
              ? Container()
              : ListTile(
                  onTap: () {
                    Navigator.of(context).pushNamed('/TuSindicato');
                  },
                  leading: Icon(
                    Icons.favorite,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Afiliación',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Asamblea');
            },
            leading: Icon(
              FontAwesomeIcons.handPaper,
              color: Colors.grey,
            ),
            title: Text(
              'Asambleas',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            trailing: Icon(Icons.chevron_right),
          ),
          ListTile(
            onTap: () {
              Navigator.of(context).pushNamed('/Tareas');
            },
            leading: Icon(
              FontAwesomeIcons.userCheck,
              color: Colors.grey,
            ),
            title: Text(
              'Tareas',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            trailing: Icon(Icons.chevron_right),
          ),
          SizedBox(height: 40),
          ListTile(
            onTap: () {
              // repo.logout().then((val) =>
              //     {Navigator.of(context).pushReplacementNamed('/Login')});
              Navigator.of(context).pushNamed('/PagoCuotaSindical');
            },
            leading: Icon(
              Icons.settings,
              color: Colors.grey,
            ),
            title: Text(
              'Pagos',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            trailing: Icon(Icons.chevron_right),
          ),
          SizedBox(),
          perfilUsuario.value.afiliado
              ? Container()
              : ListTile(
                  onTap: () {
                    api.logout(context);
                  },
                  leading: Icon(
                    Icons.exit_to_app,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Salir',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
        ],
      ),
    );
  }
}
