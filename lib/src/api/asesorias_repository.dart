import 'dart:io';
import 'package:logger/logger.dart';

import 'package:dio/dio.dart';
import './user_repository.dart';
import '../models/asesoria_model.dart';
import '../utils/helpers.dart';

var dio = Dio();
var logger = Logger(
  printer: PrettyPrinter(),
);

Future<dynamic> crearAsesoria(
    Asesoria asesoria, List<File> files, List<String> path) async {
  final uri = Helper.getUri('nueva-asesoria');
  // String fileName = file.path.split('/').last;

  FormData formData = new FormData.fromMap({
    "usuario": currentUser.value.usuario.email,
    "descripcion": asesoria.descripcion,
    "opciones": asesoria.opciones
  });

  formData.files.addAll([
    for (var file in files)
      ...{
        MapEntry(
            "fileImage",
            await MultipartFile.fromFile(file.path,
                filename: file.path.split('/').last))
      }.toList()
  ]);
  try {
    await dio.post(uri.toString(),
        data: formData,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer ${currentUser.value.token}"
            }));
    return true;
  } on DioError catch (e) {
    logger.e('RESPONSE ERR0R data 🤡---->>> $e');
    return false;
  }
}
