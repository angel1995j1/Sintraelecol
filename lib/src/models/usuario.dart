class Usuario {
  String token;
  UserD usuario;

  bool auth;

  Usuario({this.token, this.usuario});

  Usuario.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    usuario =
        json['usuario'] != null ? new UserD.fromJson(json['usuario']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    if (this.usuario != null) {
      data['usuario'] = this.usuario.toJson();
    }
    return data;
  }
}

class UserD {
  String rol;
  bool estado;
  bool facebook;
  String sId;
  String email;
  String password;
  String nombre;
  String telefono;
  String img;
  bool afiliado;
  bool pagado;

  UserD(
      {this.rol,
      this.estado,
      this.facebook,
      this.sId,
      this.email,
      this.password,
      this.nombre,
      this.telefono,
      this.img,
      this.afiliado,
      this.pagado});

  UserD.fromJson(Map<String, dynamic> json) {
    rol = json['rol'];
    estado = json['estado'];
    facebook = json['facebook'];
    sId = json['_id'];
    email = json['email'];
    password = json['password'];
    nombre = json['nombre'];
    telefono = json['telefono'];
    img = json['img'];
    afiliado = json['afiliado'];
    pagado = json['pagado'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rol'] = this.rol;
    data['estado'] = this.estado;
    data['facebook'] = this.facebook;
    data['_id'] = this.sId;
    data['email'] = this.email;
    data['password'] = this.password;
    data['nombre'] = this.nombre;
    data['telefono'] = this.telefono;
    data['img'] = this.img;
    data['afiliado'] = this.afiliado;
    data['pagado'] = this.pagado;
    return data;
  }
}
