import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../models/asesoria_model.dart';
import '../api/asesorias_repository.dart' as repoAsesorias;

class AsesoriasController extends ControllerMVC {
  GlobalKey<FormState> uploadAsesoriaFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  Asesoria asesoria = new Asesoria();
  bool fileSelected = false;

  bool uploadFile = false;

  List<File> fileAsesoria = List<File>();
  List<String> filesSelected = List<String>();

  List pathFile;

  AsesoriasController() {
    uploadAsesoriaFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void selectFile() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(allowMultiple: true);
    if (result != null) {
      List<File> file = result.paths.map((path) => File(path)).toList();

      if (file.length > 1) {
        for (var i in file) {
          filesSelected.add(i.path);
        }
        setState(() {
          fileSelected = true;
          fileAsesoria = file;
          pathFile = filesSelected;
        });
      } else {
        filesSelected.add(result.files.single.path);
        setState(() {
          fileSelected = true;
          fileAsesoria = file;
          pathFile = filesSelected;
        });
      }
    } else {
      // User canceled the picker
    }
  }

  void subirPoliza(mycontext) async {
    setState(() {
      uploadFile = true;
    });
    if (uploadAsesoriaFormKey.currentState.validate()) {
      uploadAsesoriaFormKey.currentState.save();
      var response =
          await repoAsesorias.crearAsesoria(asesoria, fileAsesoria, pathFile);
      if (response) {
        Fluttertoast.showToast(
            msg: 'Se ha enviado la asesoria',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      print('INFORMACIÓN $pathFile');
    }
    setState(() {
      uploadFile = false;
    });
  }
}
