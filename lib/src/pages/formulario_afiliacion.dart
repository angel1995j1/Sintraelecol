import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../api/user_repository.dart';
import '../controllers/afiliacion_controller.dart';

class FormularioAfliacionPage extends StatefulWidget {
  FormularioAfliacionPage({Key key}) : super(key: key);

  @override
  _FormularioAfliacionPageState createState() =>
      _FormularioAfliacionPageState();
}

class _FormularioAfliacionPageState extends StateMVC<FormularioAfliacionPage> {
  AfiliacionController _con;

  _FormularioAfliacionPageState() : super(AfiliacionController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      appBar: AppBar(
        title: Text('FORMULARIO DE AFILIACIÓN',
            style: TextStyle(color: Colors.black, fontSize: 15)),
        elevation: 0,
        centerTitle: false,
        backgroundColor: Colors.transparent,
        leading: BackButton(color: Colors.black87),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: Form(
            key: _con.afiliacionFormKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                  initialValue: currentUser.value.usuario.nombre,
                  decoration: InputDecoration(
                    labelText: 'Nombre',
                    labelStyle: TextStyle(
                      color: Theme.of(context).accentColor,
                    ),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Nombre completo',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.userCircle,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.nombre = input,
                ),
                SizedBox(height: 20),
                Container(
                    padding: EdgeInsets.all(20),
                    width: 120,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      border: Border.all(
                        width: 2,
                        color: Theme.of(context).focusColor.withOpacity(0.2),
                      ),
                    ),
                    alignment: Alignment.centerLeft,
                    child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                      hint: _con.afiliacion.tipoDocumento == null
                          ? Text('Selecciona un tipo de documento')
                          : Text(
                              _con.afiliacion.tipoDocumento,
                              style: TextStyle(
                                  color: Theme.of(context).accentColor),
                            ),
                      isExpanded: true,
                      iconSize: 30.0,
                      style: TextStyle(color: Theme.of(context).accentColor),
                      items: [
                        'Cédula de ciudadanía',
                        'Cedula de extranjería Colombiana',
                        'Pep'
                      ].map(
                        (val) {
                          return DropdownMenuItem<String>(
                            value: val,
                            child: Text(val),
                          );
                        },
                      ).toList(),
                      onChanged: (val) {
                        setState(
                          () {
                            _con.afiliacion.tipoDocumento = val;
                          },
                        );
                      },
                    ))),
                SizedBox(height: 20),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'No. Documento',
                    labelStyle: TextStyle(color: Theme.of(context).accentColor),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Número de documento',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.hashtag,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.numeroDocumento = input,
                ),
                SizedBox(height: 20),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Dirección',
                    labelStyle: TextStyle(color: Theme.of(context).accentColor),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Dirección',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.mapMarker,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.direccion = input,
                ),
                SizedBox(height: 20),
                TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Teléfono fijo',
                    labelStyle: TextStyle(color: Theme.of(context).accentColor),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Teléfono fijo',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.phone,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.telefonoFijo = input,
                ),
                SizedBox(height: 20),
                TextFormField(
                  initialValue: currentUser.value.usuario.telefono,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Teléfono móvil',
                    labelStyle: TextStyle(color: Theme.of(context).accentColor),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Teléfono móvil',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.mobileAlt,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.telefonoMovil = input,
                ),
                SizedBox(height: 20),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  initialValue: currentUser.value.usuario.email,
                  decoration: InputDecoration(
                    labelText: 'Correo electrónico',
                    labelStyle: TextStyle(color: Theme.of(context).accentColor),
                    contentPadding: EdgeInsets.all(12),
                    hintText: currentUser.value.usuario.email,
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.envelopeOpen,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.correo = input,
                ),
                SizedBox(height: 20),
                TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Edad',
                    labelStyle: TextStyle(color: Colors.black),
                    contentPadding: EdgeInsets.all(12),
                    hintText: 'Edad',
                    hintStyle: TextStyle(
                        color: Theme.of(context).focusColor.withOpacity(0.7)),
                    prefixIcon: Icon(FontAwesomeIcons.user,
                        color: Theme.of(context).accentColor.withOpacity(0.7),
                        size: 18),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color:
                                Theme.of(context).focusColor.withOpacity(0.2))),
                  ),
                  textCapitalization: TextCapitalization.words,
                  textInputAction: TextInputAction.next,
                  validator: (val) {
                    return val.isEmpty ? 'El campo es requerido' : null;
                  },
                  // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                  onSaved: (input) => _con.afiliacion.edad = input,
                ),
                const SizedBox(height: 20),
                TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Ciudad',
                      labelStyle: TextStyle(color: Colors.black),
                      contentPadding: EdgeInsets.all(12),
                      hintText: 'Ciudad',
                      hintStyle: TextStyle(
                          color: Theme.of(context).focusColor.withOpacity(0.7)),
                      prefixIcon: Icon(FontAwesomeIcons.building,
                          color: Theme.of(context).accentColor.withOpacity(0.7),
                          size: 18),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.2))),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.5))),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.2))),
                    ),
                    textCapitalization: TextCapitalization.words,
                    textInputAction: TextInputAction.next,
                    validator: (val) {
                      return val.isEmpty ? 'El campo es requerido' : null;
                    },
                    // onFieldSubmitted: () => FocusScope.of(context).requestFocus(),
                    onSaved: (input) => _con.afiliacion.ciudad = input),
                const SizedBox(height: 20),
                Row(
                  // crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    AutoSizeText(
                      'Eres directivo?',
                      minFontSize: 16,
                      maxFontSize: 18,
                    ),
                    Checkbox(
                      value: _con.directivo,
                      onChanged: (bool value) => {
                        setState(() {
                          _con.afiliacion.directivo = value;
                          _con.directivo = value;
                        }),
                        print('Es directivo ${_con.afiliacion.directivo}')
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 45,
                      width: 240,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.4),
                              blurRadius: 10,
                              offset: Offset(0, 7)),
                          BoxShadow(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.2),
                              blurRadius: 5,
                              offset: Offset(0, 3))
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(40)),
                      ),
                      child: RaisedButton(
                        color: Theme.of(context).accentColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(7)),
                        onPressed: () {
                          // Navigator.of(context).pushNamed('/AfiliacionSindical');
                          _con.registroAfiliacion(context);
                        },
                        child: Text('Siguiente',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
