import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../api/user_repository.dart';
import '../models/minuta_model.dart';
import '../api/minuta_repository.dart' as minutaRepo;

class MinutaController extends ControllerMVC {
  Minuta minuta = new Minuta();
  bool loading = false;
  String orden;
  List ordenAgregada = [];

  GlobalKey<FormState> minutaFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  MinutaController() {
    minutaFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void almacenarOrden(value) async {
    ordenAgregada.add(value);
    print('ORDEN AGREGADA $ordenAgregada');
  }

  void eliminarorden(orden) async {
    ordenAgregada.remove(orden);
  }

  void crearMinuta(String idAsamblea) {
    loading = true;
    if (minutaFormKey.currentState.validate()) {
      minutaFormKey.currentState.save();
      minuta.usuario = currentUser.value.usuario.email;
      var currDt = DateTime.now();
      var data = {
        "usuario": currentUser.value.usuario.email,
        "actaNo": minuta.actaNo,
        "fecha": minuta.fecha,
        "hora": currDt.toIso8601String(),
        "tipoAsamblea": minuta.tipoAsamblea,
        "ordenDia": ordenAgregada,
        "resumen": minuta.resumen,
        "idAsamblea": idAsamblea
      };
      minutaRepo
          .crearTarea(data)
          .then((res) => {
                if (res)
                  {
                    Fluttertoast.showToast(
                        msg: 'Se ha creado la minuta exitosamente',
                        backgroundColor: Colors.green)
                  }
              })
          .whenComplete(() => {});
    }
  }
}
