import 'package:flutter/material.dart';
import './src/models/Task.dart';
import './src/models/afiliacion_model.dart';
import './src/models/contenidoFormacion_model.dart';
import './src/models/preguntas_model.dart';
import './src/pages/Checkout.dart';
import './src/pages/contenido_formacion_detalle.dart';
import './src/pages/detallePregunta.dart';
import './src/pages/detalle_tarea.dart';
import './src/pages/pago_cuota.dart';
import './src/pages/payment_method.dart';
import './src/pages/preguntas_frecuentes.dart';
import './src/pages/procesos_organizacionales.dart';
import './src/pages/recovery_password.dart';
import './src/pages/tareas.dart';
import './src/pages/tu_afiliacion.dart';
import './src/models/route_argument.dart';
import './src/pages/afiliacion_sindical.dart';
import './src/pages/asambleas.dart';
import './src/pages/asesorias_juridicas.dart';
import './src/pages/contenidos_formacion.dart';
import './src/pages/editar_perfil.dart';
import './src/pages/formulario_afiliacion.dart';
import './src/pages/inicio.dart';
import './src/pages/login_page.dart';
import './src/pages/registro_page.dart';
import './src/pages/splashScreen_page.dart';
import './src/pages/unirse_meeting.dart';

class RouteGenerator {
  // ignore: missing_return
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/Login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/recovery-password':
        return MaterialPageRoute(builder: (_) => RecoveryPasswordPage());
      case '/Registro':
        return MaterialPageRoute(builder: (_) => RegistroPage());
      case '/Home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/PreguntasFrecuentes':
        return MaterialPageRoute(builder: (_) => PreguntasFrecuentes());
      case '/FormularioAfiliacion':
        return MaterialPageRoute(builder: (_) => FormularioAfliacionPage());
      case '/AfiliacionSindical':
        return MaterialPageRoute(
            builder: (_) =>
                AfiliacionSindicalPage(registroAfiliacion: args as Afiliacion));
      case '/PreguntaDetalle':
        return MaterialPageRoute(
            builder: (_) => PreguntaDetalle(pregunta: args as Preguntas));
      case '/ContenidoFormacionDetalle':
        return MaterialPageRoute(
            builder: (_) =>
                DetalleContenidoFormacion(contenido: args as Contenidos));
      case '/AsesoriasJuridicas':
        return MaterialPageRoute(builder: (_) => AsesoriasJuridicas());
      case '/ContenidoFormacion':
        return MaterialPageRoute(builder: (_) => ContenidosFormacion());
      case '/Asamblea':
        return MaterialPageRoute(builder: (_) => Asambleas());
      case '/UnirseReunion':
        return MaterialPageRoute(
            builder: (_) =>
                UnirseMeetingWidget(routeArgument: args as RouteArgument));
      case '/EditarPerfil':
        return MaterialPageRoute(builder: (_) => EditarPefil());
      case '/TuSindicato':
        return MaterialPageRoute(builder: (_) => TuSindicato());
      case '/ProcesosOrganizacionales':
        return MaterialPageRoute(builder: (_) => ProcesosOrganizacionales());
      case '/PagoCuotaSindical':
        return MaterialPageRoute(builder: (_) => PagoCuotaPage());
      case '/Tareas':
        return MaterialPageRoute(builder: (_) => TareasWidget());
      case '/PaymentMethod':
        return MaterialPageRoute(builder: (_) => PaymentMethodPage());
      case '/Checkout':
        return MaterialPageRoute(builder: (_) => CheckOutPage());
      case '/TareaDetalle':
        return MaterialPageRoute(
            builder: (_) => DetalleTarea(detalleTarea: args as Tareas));
    }
  }
}
