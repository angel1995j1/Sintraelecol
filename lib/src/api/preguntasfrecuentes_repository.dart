import 'dart:io';
import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';
import './user_repository.dart';
import '../models/preguntas_model.dart';

var dio = Dio();

Future getPreguntasFrecuentes() async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}preguntasFrecuentes';
  try {
    Response response = await dio.get(url,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: {
              HttpHeaders.contentTypeHeader: "application/json",
              HttpHeaders.authorizationHeader:
                  "Bearer ${currentUser.value.token}"
            }));

    return FaqsFrecuentes.fromJson(response.data);
  } catch (e) {
    print('ERROR!🚨 $e');
    return null;
  }
}
