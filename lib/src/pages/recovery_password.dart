import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/recovery_password.dart';

class RecoveryPasswordPage extends StatefulWidget {
  RecoveryPasswordPage({Key key}) : super(key: key);

  @override
  _RecoveryPasswordPageState createState() => _RecoveryPasswordPageState();
}

class _RecoveryPasswordPageState extends StateMVC<RecoveryPasswordPage> {
  RecoveryPasswordController _con;

  _RecoveryPasswordPageState() : super(RecoveryPasswordController()) {
    _con = controller;
  }
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        key: _con.scaffoldKey,
        appBar: AppBar(title: Text('Recuperar contraseña')),
        body: SingleChildScrollView(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 2.2,
              ),
              Column(
                children: [
                  Padding(
                      padding: EdgeInsets.only(top: 75),
                      child: Image.asset('./assets/img/logo.png',
                          width: 280, height: 70)),
                  Container(
                    padding: EdgeInsets.only(top: 30),
                    width: screenSize.width /
                        (2 / (screenSize.height / screenSize.width)),
                    child: Column(
                      children: [
                        Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            Container(
                                margin: EdgeInsets.only(bottom: 80),
                                width: 345,
                                child: Column(
                                  children: [
                                    SizedBox(height: 50),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      child: Form(
                                          key: _con.recoveryFormKey,
                                          child: Column(
                                            children: [
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.emailAddress,
                                                onSaved: (input) =>
                                                    _con.email = input,
                                                validator: (input) =>
                                                    !input.contains('@')
                                                        ? 'Email inválido'
                                                        : null,
                                                decoration: InputDecoration(
                                                  labelText: 'Correo',
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey),
                                                  contentPadding:
                                                      EdgeInsets.all(12),
                                                  hintText:
                                                      'Correo electrónico',
                                                  hintStyle: TextStyle(
                                                      color: Theme.of(context)
                                                          .focusColor
                                                          .withOpacity(0.7)),
                                                  prefixIcon: Icon(
                                                      FontAwesomeIcons.envelope,
                                                      color: Theme.of(context)
                                                          .accentColor),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color:
                                                              Theme.of(context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.5))),
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .focusColor
                                                                  .withOpacity(
                                                                      0.2))),
                                                ),
                                              ),
                                              SizedBox(height: 20),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    height: 45,
                                                    width: 240,
                                                    decoration: BoxDecoration(
                                                      boxShadow: [
                                                        BoxShadow(
                                                            color: Colors.green
                                                                .withOpacity(
                                                                    0.4),
                                                            blurRadius: 10,
                                                            offset:
                                                                Offset(0, 7)),
                                                        BoxShadow(
                                                            color: Colors.green
                                                                .withOpacity(
                                                                    0.2),
                                                            blurRadius: 5,
                                                            offset:
                                                                Offset(0, 3))
                                                      ],
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  40)),
                                                    ),
                                                    child: RaisedButton(
                                                      color: Colors.green,
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              new BorderRadius
                                                                  .circular(7)),
                                                      onPressed: () {
                                                        _con.sendRecoveryEmail();
                                                      },
                                                      child: _con.loading
                                                          ? CircularProgressIndicator(
                                                              backgroundColor:
                                                                  Colors.white,
                                                            )
                                                          : Text(
                                                              'Recuperar contraseña',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          )),
                                    ),
                                  ],
                                )),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
