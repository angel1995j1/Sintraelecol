import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:logger/logger.dart';

import '../models/profile_model.dart';
import '../models/Task.dart';
import '../api/user_repository.dart' as userApi;
import '../api/notas_repository.dart' as notasApi;

var logger = Logger(
  printer: PrettyPrinter(),
);

class HomeController extends ControllerMVC {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  bool loading = true;
  bool havePendigTask = false;
  // List<Tareas> tareas = <Tareas>[];
  Tasks tasks = new Tasks();

  HomeController() {
    getDataUser();
    obtenerTareas();
    // initializedNotifications();
  }

  getDataUser() async {
    ProfileUser response = await userApi.getInfoUser();
    // !response.pagado ? _showIfNotPay() : print('el usuario si pagó');
    response.email != null
        ? setState(() {
            loading = false;
          })
        : print('respinse correcta');
  }

  void obtenerTareas() async {
    dynamic task = await notasApi.obtenerTareas();
    setState(() {
      tasks = task;
    });
    if (tasks.tareas.length > 0) {
      setState(() {
        havePendigTask = true;
      });
      // _showPeriodicalNotification();
    }
  }
}
