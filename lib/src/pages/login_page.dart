import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/usuario_controller.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends StateMVC<LoginPage>
    with SingleTickerProviderStateMixin {
  UserController _con;

  _LoginPageState() : super(UserController()) {
    _con = controller;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 2.2,
            ),
            Column(
              children: [
                Padding(
                    padding: EdgeInsets.only(top: 75),
                    child: Image.asset('./assets/img/logo.png',
                        width: 300, height: 130)),
                Container(
                  padding: EdgeInsets.only(top: 10),
                  width: screenSize.width /
                      (2 / (screenSize.height / screenSize.width)),
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 80),
                            width: 345,
                            child: Column(
                              children: [
                                SizedBox(height: 50),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Form(
                                    key: _con.loginFormKey,
                                    child: Column(
                                      children: [
                                        TextFormField(
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          onSaved: (input) =>
                                              _con.email = input,
                                          validator: (input) =>
                                              !input.contains('@')
                                                  ? 'Email inválido'
                                                  : null,
                                          decoration: InputDecoration(
                                            labelText: 'Correo',
                                            labelStyle:
                                                TextStyle(color: Colors.grey),
                                            contentPadding: EdgeInsets.all(12),
                                            hintText: 'Correo electrónico',
                                            hintStyle: TextStyle(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.7)),
                                            prefixIcon: Icon(
                                                FontAwesomeIcons.envelope,
                                                color: Theme.of(context)
                                                    .accentColor),
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .focusColor
                                                        .withOpacity(0.2))),
                                            focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .focusColor
                                                        .withOpacity(0.5))),
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .focusColor
                                                        .withOpacity(0.2))),
                                          ),
                                        ),
                                        SizedBox(height: 20),
                                        TextFormField(
                                          obscureText: _con.hidePassword,
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          onSaved: (input) =>
                                              _con.password = input,
                                          validator: (input) => input.length < 3
                                              ? 'Contraseña inválida'
                                              : null,
                                          decoration: InputDecoration(
                                            labelText: 'Contraseña',
                                            labelStyle:
                                                TextStyle(color: Colors.grey),
                                            contentPadding: EdgeInsets.all(12),
                                            hintText: '••••••••••••',
                                            hintStyle: TextStyle(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.4)),
                                            prefixIcon: Icon(
                                                FontAwesomeIcons.asterisk,
                                                color: Theme.of(context)
                                                    .accentColor),
                                            suffixIcon: IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  _con.hidePassword =
                                                      !_con.hidePassword;
                                                });
                                              },
                                              color:
                                                  Theme.of(context).focusColor,
                                              icon: Icon(_con.hidePassword
                                                  ? Icons.visibility
                                                  : Icons.visibility_off),
                                            ),
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .focusColor
                                                        .withOpacity(0.2))),
                                            focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .focusColor
                                                        .withOpacity(0.5))),
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Theme.of(context)
                                                        .focusColor
                                                        .withOpacity(0.2))),
                                          ),
                                        ),
                                        SizedBox(height: 30),
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pushNamed(
                                                '/recovery-password');
                                          },
                                          child: Text(
                                            '¿Olvidaste tu contraseña? ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14),
                                          ),
                                        ),
                                        SizedBox(height: 30),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              height: 45,
                                              width: 240,
                                              decoration: BoxDecoration(
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: Colors.green
                                                          .withOpacity(0.4),
                                                      blurRadius: 10,
                                                      offset: Offset(0, 7)),
                                                  BoxShadow(
                                                      color: Colors.green
                                                          .withOpacity(0.2),
                                                      blurRadius: 5,
                                                      offset: Offset(0, 3))
                                                ],
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(40)),
                                              ),
                                              child: RaisedButton(
                                                color: Colors.green,
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(7)),
                                                onPressed: () {
                                                  setState(() {
                                                    if (!_con.loading) {
                                                      _con.login();
                                                    }
                                                  });
                                                },
                                                child: _con.loading
                                                    ? CircularProgressIndicator(
                                                        backgroundColor:
                                                            Colors.white,
                                                      )
                                                    : Text(
                                                        'Iniciar sesión',
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('¿No tienes cuenta?'),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed('/Registro');
                        },
                        child: Text(
                          " Registrate",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
