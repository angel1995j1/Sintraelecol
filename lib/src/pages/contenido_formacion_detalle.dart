// import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:video_player/video_player.dart';
import '../utils/helpers.dart';
import '../models/contenidoFormacion_model.dart';
// import 'package:social_share/social_share.dart';

// ignore: must_be_immutable
class DetalleContenidoFormacion extends StatefulWidget {
  Contenidos contenido;

  DetalleContenidoFormacion({Key key, this.contenido}) : super(key: key);

  @override
  _DetalleContenidoFormacionState createState() =>
      _DetalleContenidoFormacionState();
}

class _DetalleContenidoFormacionState extends State<DetalleContenidoFormacion> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
      widget.contenido.video,
    );
    _initializeVideoPlayerFuture = _controller.initialize();
    print(widget.contenido.detalle);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width / 20),
                  child: Padding(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width / 12),
                    child: AutoSizeText(
                      widget.contenido.titulo ?? '',
                      maxFontSize: 16,
                      minFontSize: 14,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    // child: Text(
                    //   widget.contenido.titulo,
                    //   style:
                    //       TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    // ),
                  )),
            ),
            Divider(
              indent: 190,
              height: 10,
              thickness: 2,
              color: Colors.black,
            ),
            SizedBox(height: 1),
            ListTile(
              subtitle: Center(
                child: AutoSizeText(
                  widget.contenido.subtitulo ?? '',
                  minFontSize: 14,
                  maxFontSize: 16,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Helper.applyHtml(
                context,
                widget.contenido.detalle,
              ),
            ),
            SizedBox(height: 30),
            FutureBuilder(
              future: _initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // Si el VideoPlayerController ha finalizado la inicialización, usa
                  // los datos que proporciona para limitar la relación de aspecto del VideoPlayer
                  return Container(
                    height: 180,
                    width: 290,
                    child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      // Usa el Widget VideoPlayer para mostrar el vídeo
                      child: VideoPlayer(_controller),
                    ),
                  );
                } else {
                  // Si el VideoPlayerController todavía se está inicializando, muestra un
                  // spinner de carga
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            // SizedBox(height: 25),
            // Padding(
            //   padding: EdgeInsets.only(
            //       right: MediaQuery.of(context).size.width / 6.9),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.end,
            //     children: [
            //       IconButton(
            //         icon: Icon(FontAwesomeIcons.twitter,
            //             size: 40, color: Colors.blue),
            //         onPressed: () {
            //           SocialShare.shareTwitter("Unidap",
            //                   hashtags: ["Unidapp", "appSocial", "foo", "bar"],
            //                   url: "https://google.com/#/hello",
            //                   trailingText: "\nhello")
            //               .then((data) {
            //             print(data);
            //           });
            //         },
            //       ),
            //       IconButton(
            //         icon: Icon(FontAwesomeIcons.facebookSquare,
            //             size: 40, color: Colors.blue[900]),
            //         onPressed: () {
            //           Platform.isAndroid
            //               ? SocialShare.shareFacebookStory(
            //                       './assets/img/logo.png',
            //                       "#ffffff",
            //                       "#000000",
            //                       "https://google.com",
            //                       appId: "xxxxxxxxxxxxx")
            //                   .then((data) {
            //                   print(data);
            //                 })
            //               : SocialShare.shareFacebookStory(
            //                       './assets/img/logo.png',
            //                       "#ffffff",
            //                       "#000000",
            //                       "https://google.com")
            //                   .then((data) {
            //                   print(data);
            //                 });
            //         },
            //       ),
            //       SizedBox(
            //         height: MediaQuery.of(context).size.height / 12,
            //       )
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            if (_controller.value.isPlaying) {
              _controller.pause();
            } else {
              _controller.play();
            }
          });
        },
        // Muestra el icono correcto dependiendo del estado del vídeo.
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ), // Esta coma final hace que el formateo automático sea mejor para los métodos de compilación.
    );
  }
}
