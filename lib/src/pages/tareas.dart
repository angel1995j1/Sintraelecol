import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/notas_controllers.dart';
import '../elements/DrawerWidget.dart';
import '../elements/TareaCardWidget.dart';

class TareasWidget extends StatefulWidget {
  TareasWidget({Key key}) : super(key: key);
  @override
  _TareasWidgetState createState() => _TareasWidgetState();
}

class _TareasWidgetState extends StateMVC<TareasWidget> {
  NotasController _con;

  _TareasWidgetState() : super(NotasController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.obtenerTareas();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return new IconButton(
              icon: Icon(Icons.arrow_back, color: Theme.of(context).hintColor),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('Gestor de Tareas',
                        style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Divider(
                      indent: 130,
                      endIndent: 0,
                      height: 10,
                      thickness: 3.5,
                      color: Colors.black,
                    ),
                  ],
                )),
            SizedBox(height: 30),
            Text('Tareas pendientes',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            ListView.separated(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              primary: false,
              itemCount: _con.tareas.length,
              separatorBuilder: (context, index) {
                return SizedBox(height: 20);
              },
              itemBuilder: (context, index) {
                return TareaCardWidget(tarea: _con.tareas.elementAt(index));
              },
            ),
          ],
        ),
      ),
    );
  }
}
