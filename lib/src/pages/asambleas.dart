import 'dart:io';
import 'dart:math';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jitsi_meet/jitsi_meet.dart';
import 'package:jitsi_meet/jitsi_meeting_listener.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:package_info/package_info.dart';

import 'package:share/share.dart';
import '../api/user_repository.dart';
import '../controllers/minuta_controller.dart';
import '../elements/DrawerWidget.dart';
import '../elements/PermisosDenegadosWidget.dart';
import '../elements/button_widget.dart';
import '../style/theme.dart';
import '../utils/helpers.dart';
import '../utils/jitsi_meet_utils.dart';
import '../utils/validators.dart';

import 'package:dio/dio.dart';

class Asambleas extends StatefulWidget {
  Asambleas({Key key}) : super(key: key);

  @override
  _AsambleasState createState() => _AsambleasState();
}

class _AsambleasState extends StateMVC<Asambleas> {
  MinutaController _con;
  List ordenDia = [];
  String _helperText = 'Ingresa por lo menos 1 dato';

  var _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890'; //all characters
  Random _rnd = Random();
  String randomMeetingCode; //meeting room

  _AsambleasState() : super(MinutaController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    randomMeetingCode = getRandomString(9);
    JitsiMeet.addListener(JitsiMeetingListener(
        onConferenceWillJoin: JitsiMeetUtils().onConferenceWillJoin,
        onConferenceJoined: JitsiMeetUtils().onConferenceJoined,
        onConferenceTerminated: JitsiMeetUtils().onConferenceTerminated,
        onError: JitsiMeetUtils().onError));
  }

  //Regerating Random Meeting Code
  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  @override
  void dispose() {
    super.dispose();
    JitsiMeet.removeAllListeners();
  }

  _joinMeeting() async {
    try {
      var options = JitsiMeetingOptions()
        ..room = randomMeetingCode
        ..serverURL = 'https://meet.jit.si/'
        ..subject = 'SINTRAELECOL'
        ..userDisplayName = currentUser.value.usuario.nombre
        ..userEmail = currentUser.value.usuario.email
        ..audioOnly = true
        ..audioMuted = true
        ..videoMuted = true;
      await JitsiMeet.joinMeeting(options,
          listener: JitsiMeetingListener(onConferenceWillJoin: ({message}) {
            print('${options.room} will join with message: $message');
          }, onConferenceJoined: ({message}) {
            print('${options.room} joined with message: $message');
          }, onConferenceTerminated: ({message}) {
            print('${options.room} terminated with message: $message');
          }));
      _registrarMeeting(randomMeetingCode);
    } catch (e) {
      print('🚨Error meeting');
    }
  }

  _registrarMeeting(String idAsamblea) async {
    final uri = Helper.getUri('asamblea');
    dynamic body = {
      "created_by": currentUser.value.usuario.email,
      "created_at": DateTime.now().toIso8601String(),
      "asistentes": [currentUser.value.usuario.email],
      "idAsamblea": idAsamblea
    };

    try {
      dynamic response = await dio.post(
        uri.toString(),
        data: body,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          },
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}"
          },
        ),
      );
      print('RESPUESTA DE LA API $response');
    } on DioError catch (e) {
      print('ERROR AL REGISTRAR MEETING $e');
    }
  }

  Future<String> _creatDynamiclink() async {
    // print('Información del usuario $parameter');
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String uriPrefix = "https://unidapp.page.link";

    final DynamicLinkParameters parameters = DynamicLinkParameters(
        uriPrefix: uriPrefix,
        link: Uri.parse(
            'https://unidapp.page.link/mVFa/?idMeeting=$randomMeetingCode'),
        androidParameters: AndroidParameters(
            packageName: packageInfo.packageName, minimumVersion: 1),
        iosParameters: IosParameters(
            bundleId: packageInfo.packageName,
            minimumVersion: packageInfo.version,
            appStoreId: '12456'),
        googleAnalyticsParameters: GoogleAnalyticsParameters(
            campaign: 'naxz', medium: 'social', source: 'orkut'),
        itunesConnectAnalyticsParameters: ItunesConnectAnalyticsParameters(
            providerToken: '', campaignToken: ''),
        socialMetaTagParameters: SocialMetaTagParameters(
          title: 'Únete a la reunión',
          description: 'Ingresa al link para formar parte de esta reunión',
          // imageUrl: Uri.parse(
          //     "https://firebasestorage.googleapis.com/v0/b/unidapp-5f51c.appspot.com/o/Unilogo.png?alt=media&token=6a99310d-abe7-4dd0-903c-13621a7622c6"),
        ));
    final ShortDynamicLink shortDynamicLink = await parameters.buildShortLink();
    final Uri shortUrl = shortDynamicLink.shortUrl;
    String shareText = "Unete a la reunión de : " +
        'SINTRAELECOL' +
        "\n" +
        "Abre este link: $shortUrl";
    Share.share(shareText);
    return shortUrl.toString();
  }

  @override
  Widget build(BuildContext context) {
    double screnWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: Colors.white,
      drawer: DrawerWidget(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text('Asambleas', style: TextStyle(color: Colors.black)),
        elevation: 0,
        centerTitle: true,
        leading: Builder(
          builder: (BuildContext context) {
            return new IconButton(
              icon: Icon(Icons.arrow_back, color: Theme.of(context).hintColor),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        ),
      ),
      body: !perfilUsuario.value.afiliado
          ? PermisosDenegados()
          : SingleChildScrollView(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 20, top: 40, bottom: 50, right: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Sala de reuniones',
                                ),
                                SizedBox(height: 30.0),
                                Container(
                                  height: 48.0,
                                  decoration: new BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3.0)),
                                    border: Border.all(color: Colors.grey),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            // Image.asset('assets/images/common/hash.png', scale: 3.0),
                                            Icon(FontAwesomeIcons.hashtag,
                                                size: 20),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0),
                                              child: Text(
                                                randomMeetingCode,
                                                // style: CustomTheme.textFieldTitle,
                                              ),
                                            ),
                                          ],
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            Clipboard.setData(new ClipboardData(
                                                    text: randomMeetingCode))
                                                .then((_) {
                                              showShortToast(
                                                  'Se ha copiado en el portapapeles');
                                            });
                                          },
                                          child: Icon(Icons.content_copy,
                                              color: Colors.red),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15.0),
                                /* user meeting title textField */
                                TextFormField(
                                  initialValue: currentUser.value.usuario.email,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: 'Correo',
                                    labelStyle: TextStyle(color: Colors.grey),
                                    contentPadding: EdgeInsets.all(12),
                                    hintText: 'Correo electrónico',
                                    hintStyle: TextStyle(
                                        color: Theme.of(context)
                                            .focusColor
                                            .withOpacity(0.7)),
                                    prefixIcon: Icon(FontAwesomeIcons.user,
                                        color: Theme.of(context).accentColor),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.2))),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.5))),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.2))),
                                  ),
                                ),
                                SizedBox(height: 35),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context)
                                        .pushReplacementNamed('/UnirseReunion');
                                  },
                                  child: Container(
                                    height: 45.0,
                                    decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(3.0)),
                                      border: Border.all(
                                          color: CustomTheme.primaryColor),
                                    ),
                                    child: Center(
                                        child: Text(
                                      'Unirte a reunión',
                                      style: CustomTheme.subTitleTextColored,
                                    )),
                                  ),
                                ),
                                const SizedBox(height: 15.0),
                                GestureDetector(
                                  onTap: () async {
                                    _joinMeeting();
                                  },
                                  child: HelpMe().submitButton(
                                      screnWidth, 'Crear reunión'),
                                ),
                                const SizedBox(height: 15.0),
                                GestureDetector(
                                  onTap: () {
                                    _creatDynamiclink();
                                    // Share.share(shareText);
                                  },
                                  child: Container(
                                    height: 45.0,
                                    decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(3.0)),
                                      border: Border.all(
                                          color: CustomTheme.primaryColor),
                                    ),
                                    child: Center(
                                        child: Text(
                                      'Enviar invitación',
                                      style: CustomTheme.subTitleTextColored,
                                    )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          decoration: new BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            // boxShadow: CustomTheme.boxShadow,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(left: 25, right: 25),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Agrega una minuta',
                                  style: TextStyle(fontSize: 17)),
                              SizedBox(height: 20),
                              Form(
                                key: _con.minutaFormKey,
                                child: Column(
                                  children: [
                                    TextFormField(
                                      keyboardType: TextInputType.text,
                                      onSaved: (input) =>
                                          _con.minuta.actaNo = input,
                                      decoration: InputDecoration(
                                        labelText: 'Acta No.',
                                        labelStyle:
                                            TextStyle(color: Colors.grey),
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Acta No.',
                                        hintStyle: TextStyle(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.7),
                                        ),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.5))),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.2))),
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    TextFormField(
                                      keyboardType: TextInputType.text,
                                      onSaved: (input) =>
                                          _con.minuta.fecha = input,
                                      decoration: InputDecoration(
                                        labelText: 'Fecha',
                                        labelStyle:
                                            TextStyle(color: Colors.grey),
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Fecha',
                                        hintStyle: TextStyle(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.7),
                                        ),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.5))),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.2))),
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    TextFormField(
                                      keyboardType: TextInputType.text,
                                      onSaved: (input) =>
                                          _con.minuta.tipoAsamblea = input,
                                      decoration: InputDecoration(
                                        labelText: 'Tipo',
                                        labelStyle:
                                            TextStyle(color: Colors.grey),
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Tipo de Asamblea',
                                        hintStyle: TextStyle(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.7),
                                        ),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.5))),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.2))),
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    TextFormField(
                                      keyboardType: TextInputType.text,
                                      onFieldSubmitted: (String value) {
                                        if (value.length == 0) {
                                          setState(() {
                                            _helperText =
                                                "El campo no puede estár vacío";
                                          });
                                        } else {
                                          setState(() {
                                            _con.almacenarOrden(value);
                                            ordenDia.add(value);
                                            _helperText =
                                                'Orden ${ordenDia.length}';
                                            print(ordenDia);
                                          });
                                        }
                                      },
                                      decoration: InputDecoration(
                                        helperText: _helperText,
                                        labelText: 'Orden',
                                        labelStyle:
                                            TextStyle(color: Colors.grey),
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Orden del día',
                                        hintStyle: TextStyle(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.7),
                                        ),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.5))),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.2))),
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Container(
                                        child: ordenDia.length == 0
                                            ? Center(
                                                child: Text(
                                                    'No hay ordenes agregadas'))
                                            : ListView.builder(
                                                shrinkWrap: true,
                                                physics:
                                                    NeverScrollableScrollPhysics(),
                                                itemCount: ordenDia.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return ListTile(
                                                    leading: CircleAvatar(
                                                        child: Text(
                                                            index.toString())),
                                                    title:
                                                        Text(ordenDia[index]),
                                                    trailing: IconButton(
                                                        icon: Icon(Icons
                                                            .delete_outline),
                                                        onPressed: () {
                                                          setState(() {
                                                            ordenDia.remove(
                                                                ordenDia[
                                                                    index]);
                                                            _con.eliminarorden(
                                                                ordenDia[
                                                                    index]);
                                                            _helperText =
                                                                'Orden ${ordenDia.length}';
                                                          });
                                                        }),
                                                  );
                                                },
                                              )),
                                    SizedBox(height: 20),
                                    TextFormField(
                                      keyboardType: TextInputType.text,
                                      onSaved: (input) =>
                                          _con.minuta.resumen = input,
                                      maxLines: 3,
                                      decoration: InputDecoration(
                                        labelText: 'Resúmen',
                                        labelStyle:
                                            TextStyle(color: Colors.grey),
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Resumen de la asamblea',
                                        hintStyle: TextStyle(
                                            color: Theme.of(context)
                                                .focusColor
                                                .withOpacity(0.7)),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.2))),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.5))),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .focusColor
                                                    .withOpacity(0.2))),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 10),
                              Container(
                                width: double.infinity,
                                child: RaisedButton(
                                  color: Theme.of(context).accentColor,
                                  onPressed: () {
                                    _con.crearMinuta(randomMeetingCode);
                                    ordenDia = [];
                                  },
                                  child: Text(
                                    'Agregar minuta',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              )
                            ],
                          ))
                    ],
                  )
                ],
              ),
            ),
    );
  }
}
