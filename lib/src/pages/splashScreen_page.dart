import 'package:flutter/material.dart';
import '../api/user_repository.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _animation;
  int _duration = 2000;

  @override
  void initState() {
    super.initState();
    if (_duration < 1000) _duration = 2000;
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 800));
    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.easeInCirc));
    _animationController.forward();
    loadData();
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.reset();
  }

  loadData() async {
    Future.delayed(Duration(milliseconds: _duration)).then((value) => {
          if (currentUser.value.token == null)
            {Navigator.of(context).pushReplacementNamed('/Login')}
          else
            {Navigator.of(context).pushReplacementNamed('/Home')}
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: FadeTransition(
          opacity: _animation,
          child: Center(
              child: Image.asset(
            './assets/img/logo.png',
            width: 220.0,
            height: 220.0,
          ))),
    );
  }
}
