import 'dart:convert';
import 'dart:io';
import 'package:logger/logger.dart';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/profile_model.dart';
import '../models/responseRegistro.dart';
import '../models/tu_sindicato_model.dart';
import '../models/usuario.dart';
import '../utils/helpers.dart';

var logger = Logger(
  printer: PrettyPrinter(),
);
var dio = Dio();
ValueNotifier<Usuario> currentUser = new ValueNotifier(Usuario());
ValueNotifier<ProfileUser> perfilUsuario = new ValueNotifier(ProfileUser());

Future login({@required String email, @required String password}) async {
  Uri uri = Helper.getUri('login');
  try {
    Response response = await dio
        .post(uri.toString(), data: {"email": email, "password": password});
    if (response.statusCode == 200) {
      setCurrentUser(response.data);
      currentUser.value = Usuario.fromJson(response.data);
    }

    return currentUser.value;
  } on DioError catch (e) {
    print(e);
    return e.message;
  }
}

Future<ResponseRegistro> registro(
    {@required String email,
    @required String password,
    @required String nombre,
    @required String telefono,
    @required String empresas,
    @required String sexo,
    @required String nacionalidad,
    @required String departamento}) async {
  Uri uri = Helper.getUri('registro');

  print('INFORMATION--> registration');
  try {
    Response response = await dio.post(uri.toString(), data: {
      "email": email,
      "password": password,
      "nombre": nombre,
      "telefono": int.parse(telefono),
      "empresas": empresas,
      "sexo": sexo,
      "nacionalidad": nacionalidad,
      "departamento": departamento,
      "img":
          "https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png",
      "facebook": false
    });

    print(
        'INFORMATION IN REGISTER DATA:::: Status: ${response.statusCode} Response: ${response.data} ');

    if (response.statusCode == 200) {
      print('Reponse 200');
    }
    return ResponseRegistro.fromJson(response.data);
  } on DioError catch (e) {
    print('ERROR message ${e.message}');
    print('ERROR response ${e.response}');
    return null;
  }
}

void setCurrentUser(jsonString) async {
  try {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('current_user', json.encode(jsonString));
  } catch (e) {
    throw new Exception(e);
  }
}

Future<Usuario> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (currentUser.value.auth == null && prefs.containsKey('current_user')) {
    currentUser.value =
        Usuario.fromJson(json.decode(await prefs.get('current_user')));
    currentUser.value.auth = true;
  } else {
    currentUser.value.auth = false;
  }
  // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
  currentUser.notifyListeners();
  return currentUser.value;
}

Future<void> logout(context) async {
  currentUser.value = new Usuario();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('current_user');
  Navigator.of(context).pushNamed('/Splash');
}

Future<ProfileUser> getInfoUser() async {
  Uri uri = Helper.getUri('usuario/${currentUser.value.usuario.sId}');
  try {
    Response response = await dio.get(
      uri.toString(),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          },
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}"
          }),
    );
    perfilUsuario.value = ProfileUser.fromJson(response.data);
    return ProfileUser.fromJson(response.data);
  } on DioError catch (e) {
    print(e);
    return null;
  }
}

Future sendEmailRecoveryPassword({email: String}) async {
  Uri uri = Helper.getUri('verify-email');

  print('URL $uri');
  try {
    Response response = await dio.post(uri.toString(), data: {"email": email});
    if (response.statusCode == 200) {
      return 'Te enviamos un correo con instrucciones para restablecer tu contraseña';
    }
  } catch (e) {
    print(e);
  }
}

Future<dynamic> editarUsuario(dynamic data) async {
  Uri uri = Helper.getUri('usuario/${currentUser.value.usuario.sId}');

  try {
    Response response = await dio.put(
      uri.toString(),
      data: data,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          },
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}"
          }),
    );
    print('STATUS CODE ${response.statusCode}');
    if (response.statusCode == 200) {
      getInfoUser();
      return 'Se ha actualizado correctamente';
    }
  } catch (e) {
    print('ERROR---> $e');
  }
}

Future singInWithEmailFacebook() async {}

Future<TuSindicato> afiliacion() async {
  try {
    Response response = await dio.get(
      'https://api-smc43.ondigitalocean.app/api/sindicato',
      options: Options(
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}",
        },
      ),
    );
    return TuSindicato.fromJson(response.data[0]);
  } catch (e) {
    print('error $e');
    return TuSindicato();
  }
}
