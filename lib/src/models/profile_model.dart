class ProfileUser {
  String rol;
  bool estado;
  bool afiliado;
  bool pagado;
  String sId;
  String email;
  String password;
  String nombre;
  String telefono;
  String img;

  ProfileUser(
      {this.rol,
      this.estado,
      this.afiliado,
      this.pagado,
      this.sId,
      this.email,
      this.password,
      this.nombre,
      this.telefono,
      this.img});

  ProfileUser.fromJson(Map<String, dynamic> json) {
    rol = json['rol'];
    estado = json['estado'];
    afiliado = json['afiliado'];
    pagado = json['pagado'];
    sId = json['_id'];
    email = json['email'];
    password = json['password'];
    nombre = json['nombre'];
    telefono = json['telefono'];
    img = json['img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rol'] = this.rol;
    data['estado'] = this.estado;
    data['afiliado'] = this.afiliado;
    data['pagado'] = this.pagado;
    data['_id'] = this.sId;
    data['email'] = this.email;
    data['password'] = this.password;
    data['nombre'] = this.nombre;
    data['telefono'] = this.telefono;
    data['img'] = this.img;
    return data;
  }
}
