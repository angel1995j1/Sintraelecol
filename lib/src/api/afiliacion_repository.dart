import 'dart:io';

import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';
import './user_repository.dart';
import '../models/afiliacion_model.dart';

var dio = Dio();

Future registroAfiliacion(Afiliacion afiliacion) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}formAfiliacion';

  try {
    Response response = await dio.post(
      url,
      data: afiliacion,
      options: Options(
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}",
        },
      ),
    );

    print('Response ${response.data}');
    return Afiliacion.fromJson(response.data);
  } on DioError catch (e) {
    print('Error 🚨 $e');
    return 'ERROR';
  }
}

Future updateAfiliacion(File firma, String id) async {
  final String url =
      '${GlobalConfiguration().getValue('api_base_url')}afiliacion/$id';
  try {
    Response response = await dio.put(
      url,
      data: {"firmaAfiliacion": firma},
      options: Options(
        headers: {
          HttpHeaders.contentTypeHeader: "application/json", // or whatever
          HttpHeaders.authorizationHeader: "Bearer ${currentUser.value.token}",
        },
      ),
    );

    print('Response ${response.data}');
    return Afiliacion.fromJson(response.data);
  } on DioError catch (e) {
    print('Error 🚨 $e');
    return 'ERRO';
  }
}
